# Glossary of Terms / Definitions
 - Party: the Wine-Off, hereby referred to simply as the Occasion 
 - Wine-Off: see: Party 
 - The First Snow : Defined as the first snow (where snow is rainfall (where rainfall is a substance accumulated in the troposphere, constituting at least 999,780ppm H20 by volume) of a given season reaching the ground (of any elevation) having solidified from water vapour) that reaches an unhindered depth of at least 5mm on a flat surface. (The definition of a flat surface, if disputed, may be defined in a peer reviewed paper of a respected journal, having been authored by at least two (2) members of the standards committee.) 
 - The Territory : The property owned by Pete and Tiziana Bond located at 9 de Riambochon, Sergy, France, 01630.  
 - The Interior : Inside the house located on the premises of the interior.
 - Candidate Implementation : Rule/regulation implementation committed to 
 - The Community : Those attending the Party

# Minimal/Official Party Role Assignments
 1. Host of the Occasion
 2. Guest to the Host of the Occasion
 3. Reference Implementation Chair
 4. Sommelier #1
 5. Sommelier #2
 6. Convener of Non-alcoholic Refreshments
 7. Mulled Wine Task Force Chair
 8. Mulled Wine Task Force Consultant
 9. Judge
 10. Bailiff
 11. Clerk
 12. Counsel for the Defense of the Rules
 13. Counsel for the Plaintiff
 14. Ethics Committee Chair
 15. Ethics Member #1
 16. Ethics Member #2
 17. Search Committee Member 1 → Judiciary Committee Member #1
 18. Search Committee Member 2 → Judiciary Committee Member #2
 19. Cultural Chair of Auditory Excitation
 20. Technical Chair of Auditory Excitation
 21. Master of Ceremonies of Festivus

# Pre-Party Assignment Assignments
The Host of the Occasion is the owner of the territory where the Wine-off shall take place.  The Host of the Occasion is responsible for developing a candidate reference implementation of the rules prior to the Party.  The search committee shall comprise three (3) members who shall be selected during the ad-hoc midday nourishment board immediately proceeding the first snow of the current season, or the first midday temperature above 50C of the current season.  All remaining assignments shall be made at 18h00 on the day of the Wine-off at the territory.  The reference implementation of the rules shall be fully developed prior to the party by the Reference Implementation Chair.

# Assignment Assignments Prior to the Occasion
The head of the ethics committee shall nominate four (4) members to the ethics committee. Two (2) nominees shall be promoted to members, who, together with the chair, shall comprise the ethics committee, and the final two (2) nominees shall be appointed as Bailiff and Clerk (alphabetically). In dispute of who is which, the responsibility of allocating seats to the ethics committee shall be taken upon by the search committee who shall review the candidates by simple plurality vote.  Once formed, the Ethics Committee is required to confirm the appointment of the Search Committee as Judiciary Committee, adding any members deemed necessary, providing the appointment can be shown to be sufficiently unethical.

# Assignment Assignments of the Occasion (18h00)
The Host of the Occasion shall make all nominations executively.  All nominations must be approved with a simple plurality vote by the ethics committee.  All unfilled appointments must be allocated.  Every person in attendance of the Occasion must have at least one appointment.

# Reference Implementation
The Reference Implementation Chair shall serve as the Owner of the reference implementation which must ultimately be approved by the ethics committee with a simple majority vote. The Reference Implementation Chair shall review the current implementation incorporating suggestions by members of the Community.  If a suggestion is disputed, the Judiciary Committee shall adjudicate and determine whether it is to be implemented.  The Counsel for the Defense of the Rules shall argue the conservation of the existing state of the rules and the Counsel for the Plaintiff shall advocate to implement the new rule or the divergence of an existing rule.  Once the candidate reference implementation is promoted to nominative reference implementation it shall be signed by an appropriate authority (such as the CERN Certificate Authority 2), and distributed amongst the invitees. The reference implementation shall consist of an architecture ambiguous docker image in the GitLab Continuous Integration (CI) model.

# Defining the Rules
Upon completion of the formation of the above committees, the rules for the competition of the Occasion itself shall proceed to be defined.  These rules shall be implemented by the Reference Implementation Chair.

# The Occasion Attendance Rules
- All participants to the Occasion shall provide their own glass for drinking the wine. The first six (6) attendees to arrive absent a glass shall be granted use of an IKEA DUKTIG glass (colour of their choosing). Further attendees without glasses shall be required to hold the wine in their hands.
- All rules of the Occasion shall strictly have a literal interpretation when applied during the proceedings.

# The Competition Rules
- This is a simple scoring system based on that of the ancient Macedonians.
- Write your name on the label, or somehow, on both bottles of wine before bringing it to the party.
- Wine tasting is to commence at 19h00 and such proceedings will be opened by the Master of Ceremonies.
    - This assumes that people being told to show up at 6pm will show up at 6:30pm, giving us ample time to prepare.
- Bottles will be wrapped/blinded in the same color paper and labelled in pairs using a common symbol (probably numbers). This will be done in a way such that those who wrap/label the bottles are also blind to their contents.  This will be achieved in two steps :
    - [1] Sommelier #1 will obtain all bottles at the beginning and wrap them, passing each pair in turn to Sommelier #2.
    - [2] Sommelier #2 will then label the bottle with a unique symbol (probably a number) being sure to do so in a random order.
- A score card shall be associated to each bottle.  At this time the bottle will be opened and served for tasting.
- When you taste a wine, you should then write on this score card the score that you give this bottle, which may be an integer or irrational number only, and the current total score (see below for how to tally points).
    - Scores can range from [0,5] from 19h00-20h00.
    - Scores can range from [-5,5] from 20h00 onward.
- The endgame
    - When a bottle is empty, it can no longer be scored.
    - When all bottles are empty, or no participants can pass a sobriety test, the competition stops.
- The bottle ranked first wins, the bottle ranked second gets second place, and (you guessed it) the bottle ranked third gets third place.
- After tallying the points on the back of each empty bottle, reveal (with a drumroll) the bottle by removing the paper.  
- Scoring fine print - for the observant wino :
    - All participants to the Occasion are granted two (2) bonus votes, to be applied to any bottle(s) of their choosing, in the range of [-2,2]
    - Any participants heard by any other participant using "fluid ounces" as a unit of volume or mass by weight shall forfeit any remaining bonus votes in their possession.
    - If at any point, you notice that a bottle’s total score (before you score it) is a prime number, you can choose to [1] Subtract 10 points (or) [2] Double its score 
        - Example : 7
    - If at any point, a bottles score is greater than 30, halve that bottles score [decimal values shall be rounded up].  If the score reaches 30 a second time, then the bottle's score can continue up without a ceiling.
        - Example : If a bottle with a score of 29 is given 4 points, then its score would become (29+4)/2 = 16.5 → (round up) → 17
    - If you can successfully do a one-armed pushup and have it validated by someone who has at least 80% of the total surface area of their finger nails painted, then you can give up to 10 bonus points distributed as you wish to the bottles.
        - Example : Bro, do you even lift?
    - If you can demonstrably prove that you are Philigree Shencoua, then you can give out 10 bonus points distributed as you wish to the bottles.
    - If you can demonstrably prove you are not Philigree Shencoua, then you may continue to participate in the competition. Participants to the Occasion may be challenged on their being Philigree Shencoua at any time, up to a maximum of one time(s).
    - If you can demonstrably prove that you believe in Santa Claus, then you may give out 20 bonus points distributed as you wish to the bottles. These bonus points shall not be tallied in the final scores.
- Prizes - There will be a duplicate of each bottle of wine.  The winners will have their selection from among these bottles.  The bottles will be ranked as follows :
    - First place : Bottle with the most total points when each score is added together, positive or negative. Prize: Select up to 6 bottles of wine.
    - Second place (after excluding the first place bottle) : Bottle with the most “integrated votes” which are summed as one point for each positive score, and zero points for a score equal to or less than zero. Prize: Select up to 5 bottles of wine.
    - Third place (after excluding the first or second place bottles) : The smallest non-negative score. Prize: Select up to 4 bottles of wine.
    - Fourth place (after excluding the first, second, or third place bottles) : The smallest overall score, whether that be positive or negative. Prize: Select up to 3 bottles of wine.
    
